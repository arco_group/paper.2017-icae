;;; This file contains some of the relationships that are requirered
;;; for de case scenarios devised for the Smart Grid

(new-relation {is in contact with}
	      :a-inst-of {thing}
	      :b-inst-of {thing})

(new-eq {is in contact with} {located at})

